package com.example.recyclerview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.StudentHoler> {
    private List<Student> mListStudent = new ArrayList<>();
    private OnActionCallBack mCallBack;

    public StudentAdapter(List<Student> mListStudent) {
        this.mListStudent = mListStudent;
    }

    public void setmCallBack(OnActionCallBack mCallBack) {
        this.mCallBack = mCallBack;
    }

    @NonNull
    @Override
    public StudentHoler onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_student, parent, false);
        StudentHoler holer = new StudentHoler(view);
        return holer;
    }

    @Override
    public void onBindViewHolder(@NonNull StudentHoler holder, int position) {
        Student item = mListStudent.get(position);
        holder.tvName.setText(item.getName());
        holder.tvId.setText("Ma HS: " + item.getId() +
                " / Lop: " + item.getClassName());
        holder.tvAge.setText(item.getAge() + "");
        holder.imvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mCallBack != null) {
                    mCallBack.deleteItem(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mListStudent.size();
    }

    public class StudentHoler extends RecyclerView.ViewHolder {
        TextView tvAge, tvName, tvId;
        ImageView imvDelete;
        public StudentHoler(@NonNull View itemView) {
            super(itemView);
            initViews(itemView);
        }

        private void initViews(View itemView) {
            tvAge = itemView.findViewById(R.id.tv_age);
            tvName = itemView.findViewById(R.id.tv_name);
            tvId = itemView.findViewById(R.id.tv_id);
            imvDelete = itemView.findViewById(R.id.imv_delete);
        }
    }

    public interface OnActionCallBack{
        void deleteItem(int pos);
    }
}
