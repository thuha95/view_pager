package com.example.recyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private RecyclerView rvStudent;
    private List<Student> listStudent = new ArrayList<>();
    private List<Student> listStudentClone = new ArrayList<>();
    private EditText edtSearch;
    private StudentAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        initDummyData();
    }

    private void initDummyData() {
        Student student1 = new Student("HS001","Nguyen ABC", "Math001", 18);
        Student student2 = new Student("HS002","Nguyen BDE", "Math001", 18);
        Student student3 = new Student("HS003","Nguyen BCG", "Math001", 18);
        Student student4 = new Student("HS004","Nguyen DHF", "Math001", 18);
        Student student5 = new Student("HS005","Nguyen EBC", "Math001", 18);
        Student student6 = new Student("HS006","Nguyen FQD", "Math001", 18);
        Student student7 = new Student("HS007","Nguyen G", "Math001", 18);
        Student student8 = new Student("HS008","Nguyen HCD", "Math001", 18);
        Student student9 = new Student("HS009","Nguyen OBC", "Math001", 18);
        Student student10 = new Student("HS0010","Nguyen M", "Math001", 18);
        Student student11 = new Student("HS0011","Nguyen BCN", "Math001", 18);
        listStudent.add(student1);
        listStudent.add(student2);
        listStudent.add(student3);
        listStudent.add(student4);
        listStudent.add(student5);
        listStudent.add(student6);
        listStudent.add(student7);
        listStudent.add(student8);
        listStudent.add(student9);
        listStudent.add(student10);
        listStudent.add(student11);

        listStudentClone.clear();
        listStudentClone.addAll(listStudent);
    }

    private void initViews() {
        rvStudent = findViewById(R.id.rv_student);
        // 1 cot nhieu hang or 1 hang nhieu cot thi dung LinearLayoutManager
        // nhieu hang nhieu cot: GridLayoutManager
        LinearLayoutManager manager = new LinearLayoutManager(this,RecyclerView.VERTICAL,false);
        rvStudent.setLayoutManager(manager);
        adapter = new StudentAdapter(listStudent);
        adapter.setmCallBack(new StudentAdapter.OnActionCallBack() {
            @Override
            public void deleteItem(int pos) {
                listStudent.remove(pos);
                adapter.notifyDataSetChanged();
            }
        });
        rvStudent.setAdapter(adapter);

        edtSearch = findViewById(R.id.edt_search);
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s == null || s.toString().isEmpty()){
                    listStudent.clear();
                    listStudent.addAll(listStudentClone);
                    adapter.notifyDataSetChanged();
                    return;
                }

                if(s.toString().length() > 1) {
                    doSearch(s.toString());
                }
            }
        });
    }

    private void doSearch(String key) {
        listStudent.clear();
        for (Student student: listStudentClone){
            if(student.getName().contains(key)){
                listStudent.add(student);
            }
        }
        adapter.notifyDataSetChanged();
    }
}